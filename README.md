# Introduction

## The original cube

[Rubik's Cube](https://en.wikipedia.org/wiki/Rubik%27s_Cube) is an
amazing toy: It is a mechanical [permuation
puzzle](https://en.wikipedia.org/wiki/Combination_puzzle) with an
incredible complex underlying non-commutative 
[group](https://en.wikipedia.org/wiki/Rubik%27s_Cube_group).

In its original $`3{\times}3{\times}3`$ form it has 
```math
  \frac{12! \cdot 2^{12}\cdot 8! \cdot 3^8}{2\cdot2\cdot3} 
  = 43\, 252\, 003\, 274\, 489\, 856\, 000
```
possible orientations. This number is slightly (no considerably) more than $`43\times 10^{18}`$ 
or 43 quintillion and really huge!

Despite of its complexity there exist a number of strategies to solve
the original cube. (And I owe an eternal debt to Kurt Endl who devised
an ingenious solution strategy based on just three very intuitive
moves – which are therefore difficult to forget.)

## Extensions

Once one has mastered the original $`3{\times}3{\times}3`$ cube further
challenges await – among others:
1. Rubik's cubes with more layers (can all be tackled by 
   [Endl's method](https://books.google.de/books/about/Die_gemeinsame_Strategie_f%C3%BCr_alle_Rubik.html)),
2. solving the cube as fast as possible
   ([*speedcubing*](https://en.wikipedia.org/wiki/Speedcubing), which
   is quite popular but not my cup of tea),
3. solving the cube with as few moves as possible,
   a. using a computer,
   b. without the help of a computer,
4. solving the cube blindfolded,
5. solving the puzzle in higher dimensions.

## Cubes in higher dimensions (hypercubes)

The cube is one of three [regular
polytopes](https://en.wikipedia.org/wiki/Regular_polytope) that exist
in any dimension. (The other two are the
[simplex](https://en.wikipedia.org/wiki/Hypersimplex) and its
[Platonic
sibling](http://www.georgehart.com/virtual-polyhedra/platonic_relationships.html),
the [cross-polytope](https://en.wikipedia.org/wiki/Cross-polytope)).

An $`n+1`$ dimensional hypercube can be constructed from an $`n`$
dimensional one by extrusion:

![An animation showing how to create a tesseract from a point.](https://upload.wikimedia.org/wikipedia/commons/d/d9/From_Point_to_Tesseract_%28Looped_Version%29.gif) (Vitaly Ostrosablin, [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0))

### Number of elements

The extrusion process implies the following laws:
- The number of *vertices* doubles.
- The number of *edges* doubles and and additionally increases by the
  number of vertices of the lower dimensional polytope.
- The number of *faces* doubles and and additionally increases by the
  number of edges of the lower dimensional polytope.
- And so on for the number of *cells* and hihger polytopes with a
  dimension less than $`n`$ (with $`n`$ being the dimensionality of
  the resulting hypercube).
- There is exactly one $n$-polytope.
- There are no elements with a dimension higher than $`n`$.

| dinension | name      | vertices | edges | faces | cells | 4-polytopes | 5-polytopes |
| --------- | ----      |  ------- | ----- | ----- | ----- |  ---------- | ----------- |
|         0 | point     |        0 |     0 |     0 |     0 |           0 |           0 |
|         1 | line      |        2 |     1 |     0 |     0 |           0 |           0 |
|         2 | square    |        4 |     4 |     1 |     0 |           0 |           0 |
|         3 | cube      |        8 |    12 |     6 |     1 |           0 |           0 |
|         4 | hypercube |       16 |    32 |    24 |     8 |           1 |           0 |
|         5 |           |       32 |    80 |    80 |    40 |          10 |           1 |
|         6 |           |       64 |   192 |   240 |   160 |          60 |          12 |

The general formula for the number of $`k`$-polytopes in a $`n`$-hypercube is
```math
E_{k,n} = {n\choose k} 2^{n-k}.
```

### Graphs

A hypercube can be projected into a plane by a [skew orthonogonal projection](https://en.wikipedia.org/wiki/Petrie_polygon#The_hypercube_and_orthoplex_families) [1].

![line segment](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/1-simplex_t0.svg/240px-1-simplex_t0.svg.png)
![square](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/2-cube.svg/240px-2-cube.svg.png)
![cube](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/3-cube_graph.svg/240px-3-cube_graph.svg.png)
![tesseract](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/4-cube_graph.svg/240px-4-cube_graph.svg.png)
![5-cube](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/5-cube_graph.svg/240px-5-cube_graph.svg.png)

## Rubik's cubes in higher dimensions

The 6 faces of a 3-cube can be represented by a cube net (by Wikipedia
contributor [Rectas](https://commons.wikimedia.org/wiki/User:Rectas)).

![Net of a cube](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Cubo_desarrollo.gif/299px-Cubo_desarrollo.gif)

(In fact, there are eleven ways to flatten a cube: ![the 11 nets of
the
cube](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/The_11_cubic_nets.svg/250px-The_11_cubic_nets.svg.png).)

Instead of 6 square faces the 4-cube unfolds to 8 cubic cells. The net
of a 4-cube can be found on Wikipedia as well: 
![Net of a 4-cube](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Net_of_tesseract.gif/150px-Net_of_tesseract.gif)
(張宇帆, [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/3.0))


In the $`3{\times}3{\times}3{\times}3`$ Rubik's cube is divided intog
$`3^4=81`$ small 4-cubes, the hypersurfaces of which contain eight
small cubes (its *cells*) each. There is one uncolored one at the
chenter, 16 with four colored small cells, 32 with 3 colored cells, 24
with two colored cells and 8 with one colored cell (Yoshino 2017).

| type   | colord squares | small cubes | colored cells | small 4-cubes |
| ----   | -------------- | ----------- | ------------- | ------------- |
| cell   | -              | -           |             1 |             8 |
| face   | 1              | 6           |             2 |            24 |
| edge   | 2              | 12          |             3 |            32 |
| vertex | 3              | 8           |             4 |            16 |
| total  |                | 26          |               |            80 | 

The total number of colored small cells is therefore
```math
  8\cdot 1 + 24\cdot 2 + 32\cdot 3 + 16\cdot 4 = 216 = 8\cdot 27
```

A 3D Rubik's cube is solved, if the nine stickers on each of the six faces have the same color.
A 4D Rubik's cube is solved, if the 27 subcubes of the eight cubic cells have the same color.

### Number of permutations

The number of possible positions of the cubelets can be calculated (Balandraud, ?).
- The eight 1-colored elements are immobile.
- Every move changes the parity of the 2-colored and the 3-colored elements the same way.
  Thus, of the $`24!`$ permutations of the 2-colored elements and the $`32!`$
  permutations of the 3-colored elements only those with the same
  parity are possible ($`24!\cdot 32!/2`$).
- The parity of the 4-colored elements is always even ($`16!/2`$).
- Every 2-colored piece can have $`2`$ orientations in one place; the orientation of the
  last one is determined by the orientation of the other 23 ($`(2!)^{24}/2`$).
- Every 3-colored piece can have $`3!`$ orientationes in one place,
  except for the last one, which can have only 3 positions ($`(3!)^{32}/2`$).
- The 4-colored element can have $`4!/2`$ orientations in one place,
  except for the last one, which can have only 4 positions 
  
The total number of reachable positions for the $`3{\times}3{\times}3{\times}3`$ cube amounts to
a whopping
```math
  \frac{24!\cdot 32!}{2} \cdot \frac{16!}{2}
  \cdot \frac{(2!)^{24}}{2}
  \cdot \frac{(3!)^{32}}{2}
  \cdot \frac{(4!/2)^{16}}{3}
  = 1\, 756\, 772\, 880\, 709\, 135\, 843\, 168\, 526\, 079\, 081\,
        025\, 059\, 614\, 484\, 630\, 149\, 557\, 651\, 477\, 156\,
        021\, 733\, 236\, 798\, 970\, 168\, 550\, 600\, 274\, 887\,
        650\, 082\, 354\, 207\, 129\, 600\, 000\, 000\, 000\, 000
```
```math
  \frac{24!\cdot 32!}{2} \cdot \frac{16!}{2}
  \cdot \frac{(2!)^{24}}{2}
  \cdot \frac{(3!)^{32}}{2}
  \cdot \frac{(4!/2)^{16}}{3}
  = 1 756 772 880 709 135 843 168 526 079 081 025 059 614 484 630 149 557 651 477 156 021 733 236 798 970 168 550 600 274 887 650 082 354 207 129 600 000 000 000 000
```
or roughly $`10^{120}`$.




